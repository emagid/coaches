<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package emagid
 */

?>

	<!-- FOOTER SECTION STARTS -->
<div class="social_links_footer">
    <h3>Follow me at</h3><br>
                    <ul>

                        <li>
                            <a href="https://www.facebook.com/Chuck-Cecil-194356730589518/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/facebook-letter-logo.png"></a>
                        </li>
                        <li>
                            <a href="https://twitter.com/chuckcecil26" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/twitter-logo.png"></a>
                        </li>
                        <li>
                            <a href="https://www.linkedin.com/in/chuck-cecil-coach" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/linkedin-logo.png"></a>
                        </li>
                                                <li>
                            <a href="https://www.youtube.com/channel/UC61ndNDvremkqv3fqVUtAdg?view_as=subscriber" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/youtube-logo.png"></a>
                        </li>



                        
                    </ul>
                </div>
<!--
	<footer>

		<div class='footer_holder'>
            		 		<a href="/" class="logo">
            <h1>Chuck Cecil</h1>
            </a>
			<div class="links">
                <div class="social_links_footer">
                    <ul>
                        <li>
                            <a href="https://www.facebook.com/eugene.remm?fref=ts" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/facebook.png"></a>
                        </li>
                        <li>
                            <a href="https://twitter.com/Eugene_Remm" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/twitter.png"></a>
                        </li>
                        <li>
                            <a href="https://vimeo.com/user51078408%20" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/vimeo.png"></a>
                        </li>
                        <li>
                            <a href="https://www.instagram.com/eugeneremm" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/instagram.png"></a>
                        </li>
                        <li>
                            <a href="https://www.linkedin.com/in/eugene-remm-77b2506b" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/linkedin.png"></a>
                        </li>



                        
                    </ul>
                </div>
			</div>
			
            
		</div>
	</footer>
-->
	<!-- FOOTER SECTION ENDS -->


	<script>
    $('a[href*="#"]').on('click', function (e) {
	e.preventDefault();

	$('html, body').animate({
		scrollTop: $($(this).attr('href')).offset().top
	}, 500, 'linear');
});
</script>

<?php wp_footer(); ?>

</body>
</html>
