<?php
/**
 * The template for displaying all pages
 *
 *  Template Name: Front Page
 * 
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package emagid
 */

get_header('home'); ?>



	<!-- HERO SECTION -->
	<section class='hero home_hero' style="background-image:url(<?php the_field('banner'); ?>);">
        <div class="overlay">
            <div class='text_box'>
                <h1>Coach Chuck Cecil</h1>
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ncaa-logo.png">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/nfl-logo_alt.png">
            </div>
        </div>
	</section>

	<!-- HERO SECTION END -->



	<!-- INTRO SECTION -->
	<section class='intro'>
        <div class="container">
            <div>
                <h1><?php the_field('intro_header'); ?></h1>

                <div class="intro_about">
                    <?php the_field('intro_section'); ?>
                    </div>
                          
            </div>

        
        </div>
	</section>
	<!-- INTRO SECTION END -->

<!-- SOCIAL SECTION  -->
<section>
    <div class="image_gallery">
        <?php echo do_shortcode('[URIS id=799]'); ?>
    </div>
    <div class="home_readmore">
    <a href='/about' class='button'> Read More </a>
    </div>
</section>

<!-- SOCIAL SECTION END  -->




<section class="podcasts" id="press">
<!--CLIENTS SECTION-->
<!-- Slider main container -->
<div class="podcast_Wrapper">
    <h1>COACH’S VIDEO BOOTH</h1>
    <!-- Additional required wrapper -->
    <div class="">
                    <?php
	  			$args = array(
	    		'post_type' => 'podcasts',
                    'posts_per_page' => 99
	    		);
	  			$products = new WP_Query( $args );
	  				if( $products->have_posts() ) {
	    			while( $products->have_posts() ) {
	      		$products->the_post();
			?> 
        
        <div class="podcasts_videos">
            <?php the_field('content'); ?>
        </div>
        
        <?php
			}
				}
			else {
			echo 'No Projects Found';
			}
		?>  
        <?php wp_reset_query();	 // Restore global post data stomped by the_post(). ?>
    </div>
    <!-- If we need pagination -->
<!--    <div class="swiper-pagination swiper-pagination2"></div>-->

    <!-- If we need navigation buttons -->
<!--
    <div class="swiper-button-prev"></div>
    <div class="swiper-button-next"></div>
-->

</div>
    
    
</section>



	<!-- INTRO SECTION -->
	<section class='intro'>
        <div class="container">
            <div>
                <h1>Philanthropic Efforts</h1>

                <div class="intro_about">
                    <?php the_field('philanthropy_text'); ?>
                    </div>

            </div>

        
        </div>
	</section>
	<!-- INTRO SECTION END -->



	<!-- INTRO SECTION -->
	<section class='intro' id="greyed_out">
        <div class="container">
            <div>
                <h1>Chuck's Coaching Career</h1>

                <div class="career">
                                        <div class="team">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/wildcats.png">
                        <p><strong>2016 - Current</strong></p>
                        <p>University of Arizona </p>
                    </div>
                                        <div class="team">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/st_louis_rams.png">
                        <p><strong>2012 - 2016</strong></p>
                        <p>St. Louis/Los Angeles Rams </p>
                    </div>
                    <div class="team">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/tennessee_titans.png">
                        <p><strong>2001 - 2010</strong></p>
                        <p>Tennessee Titans </p>
                    </div>


                </div>

            </div>
            
            

        
        </div>
        
                <div class="container">
            <div>
                <h1>Chuck's Playing Career</h1>

                <div class="career player">
                    <div class="team">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/houston-oilers.png">
                        <p><strong>1995</strong></p>
                        <p>Houston Oilers</p>
                    </div>
                    <div class="team">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/arizona_cardinals.png">
                        <p><strong>1993</strong></p>
                        <p>Phoenix Cardinals </p>
                    </div>
                    <div class="team">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/greenbay.png">
                        <p><strong>1988-1992</strong></p>
                        <p>Green Bay Packers </p>
                    </div>
                    
                                        <div class="team">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/wildcats.png">
                                            <p><strong>1985-1988</strong></p>
                        <p>University of Arizona</p>
                    </div>
                </div>

            </div>
            
            

        
        </div>
	
	<!-- INTRO SECTION END -->

	<!-- INTRO SECTION -->
        <div class="container">
            <div>
                <h1>Honors & Awards</h1>

                <div class="career awards">
                    <div class="team">
                        <p><strong>2010</strong></p>
                        <p>NCAA College Football Hall of Fame Induction</p>
                    </div>
                    <div class="team">
                        <p><strong>1993</strong></p>
                        <p>University of Arizona Sports Hall of Fame Inductee</p>
                    </div>
                                        <div class="team">
                        <p><strong>1992</strong></p>
                        <p>NFL Pro Bowl Selection</p>
                    </div>
                                        <div class="team">
                        <p><strong>1991</strong></p>
                        <p>NFL All-Madden Selection</p>
                    </div>
                    
                                        <div class="team">
                        <p><strong>1988</strong></p>
                        <p>Pac-10 Conference Medal winner</p>
                    </div>
                    <div class="team">
                        <p><strong>1988</strong></p>
                        <p>NCAA Today's Top VI Award recipient</p>
                    </div>
                                        <div class="team">
                        <p><strong>1987</strong></p>
                        <p>Consensus All-American </p>
                    </div>
                                        <div class="team">
                        <p><strong>1987</strong></p>
                        <p>UPI, Football News, Kodak, Walter Camp and coaches All-America selection</p>
                    </div>
                    
                                                            <div class="team">
                        <p><strong>1986-87</strong></p>
                        <p>Jim Thorpe Award Semifinalist</p>
                    </div>
                    <div class="team">
                        <p><strong>1987</strong></p>
                        <p>Pac-10 Defensive Player of the Year</p>
                    </div>
                                        <div class="team">
                        <p><strong>1986, 1987</strong></p>
                        <p>2-time Academic All-America first team </p>
                    </div>
                    
                                                            <div class="team">
                        <p><strong>1986, 1987</strong></p>
                        <p>2-time All-Pac-10 first team</p>
                    </div>
                    
                    <div class="team">
                        <p><strong>1986</strong></p>
                        <p>Aloha Bowl MVP </p>
                    </div>
                    
                    <div class="team">
                        <p><strong>1985</strong></p>
                        <p>Academic All-America second team</p>
                    </div>
                </div>

            </div>
            
            

        
        </div>

	<!-- INTRO SECTION END -->
      </section>

  <script>
  var swiper2 = new Swiper ('.swiper2', {

          // If we need pagination
    pagination: {
      el: '.swiper-pagination2',
    },
    // Optional parameters
    loop: true,
      slidesPerView: 1,
      spaceBetween: 40,
//      centeredSlides: true,
        navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
//      breakpoints: {
//        1024: {
//          slidesPerView: 4,
//          spaceBetween: 40,
//        },
//        768: {
//          slidesPerView: 3,
//          spaceBetween: 30,
//        },
//        640: {
//          slidesPerView: 2,
//          spaceBetween: 20,
//        },
//        320: {
//          slidesPerView: 1,
//          spaceBetween: 10,
//        }
//      }
  });

  </script>

<script>
      
        var swiper1 = new Swiper ('.swiper1', {
        autoplay: {
        delay: 5000,
        centeredSlides: true,
  },
    // Optional parameters
//    loop: true,
      slidesPerView: 1,

    // If we need pagination
    pagination: {
      el: '.swiper-pagination1',
    },
  });
    
    $(".arrow_circle#one").click(function(){
        $(this).slideToggle();
        $(".video1").addClass('vwvh_100');
    $(".text_half#light_mode").animate({width:'toggle'},550);
});
        $(".arrow_circle#two").click(function(){
        $(this).slideToggle();
            $(".video2").addClass('vwvh_100');
    $(".text_half#dark_mode").animate({width:'toggle'},550);
});
    
    var vid = document.getElementById("video1");

function enableMute() { 
  vid.muted = true;
} 

function disableMute() { 
  vid.muted = false;
}
    
 $("#video1").prop('muted', true);

  $("#mute-video1").click( function (){
    if( $("#video1").prop('muted') ) {
          $("#video1").prop('muted', false);
    } else {
      $("#video1").prop('muted', true);
    }
  });  
    
     $("#video2").prop('muted', true);

  $("#mute-video2").click( function (){
    if( $("#video2").prop('muted') ) {
          $("#video2").prop('muted', false);
    } else {
      $("#video2").prop('muted', true);
    }
  }); 
    
         $("#video3").prop('muted', true);

  $("#mute-video3").click( function (){
    if( $("#video3").prop('muted') ) {
          $("#video3").prop('muted', false);
    } else {
      $("#video3").prop('muted', true);
    }
  }); 
    
             $("#video4").prop('muted', true);

  $("#mute-video4").click( function (){
    if( $("#video4").prop('muted') ) {
          $("#video4").prop('muted', false);
    } else {
      $("#video4").prop('muted', true);
    }
  }); 
    
$("#video5").prop('muted', true);

  $("#mute-video5").click( function (){
    if( $("#video5").prop('muted') ) {
          $("#video5").prop('muted', false);
    } else {
      $("#video5").prop('muted', true);
    }
  }); 
</script>

<?php
get_footer();
