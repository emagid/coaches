<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package emagid
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    
	<link rel="profile" href="http://gmpg.org/xfn/11">
    
<!--    CUSTOM STYLE-->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/urock.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/fonts/proximaNova/font.css">
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.min.css">
    
    
<!--    CUSTOM SCRIPT-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/core.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js"></script>

<!--SLICK SLIDER-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.1/css/swiper.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.1/js/swiper.min.js"></script>
    

    
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<!-- NAVIGATION BAR MENU -->
	<header>
		<a href="/" class="logo">
            <h1><img src="<?php echo get_template_directory_uri(); ?>/assets/img/football.png"></h1>
            </a>
		<nav>
            <?php
                    wp_nav_menu( array(
                        'theme_location' => 'menu-1'
                    ) );
                ?>
		</nav>

		<!-- Mobile -->
<!--
		<div id="hamburger">
            <div class="bar1 hamburger_line"></div>
            <div class="bar2 hamburger_line"></div>
            <div class="bar3 hamburger_line"></div>
        </div>
-->

<!--
        <div class='dropdown'>
            <//?php
                    wp_nav_menu( array(
                        'theme_location' => 'menu-1'
                    ) );
                ?>
        </div>
-->
        <!-- Mobile Ends -->
	</header>
	<!-- NAVBAR ENDS -->
