<?php

get_header('home'); ?>



	<!-- HERO SECTION -->
	<section class='hero home_hero' style="background-image:url(<?php the_field('banner'); ?>)">
        <div class="overlay">
            <div class='text_box'>
                <h1><?php the_field('intro_header'); ?></h1>
<!--                <a href='' class='button'> Read More </a>-->
            </div>
        </div>
	</section>

	<!-- HERO SECTION END -->




	<!-- INTRO SECTION -->
	<section class='intro' id="greyed_out">
        <div class="container">
            <div>
                <h1>Chuck's Coaching Career</h1>

                <div class="career">
                                        <div class="team">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/wildcats.png">
                        <p><strong>2016 - Current</strong></p>
                        <p>University of Arizona </p>
                    </div>
                                        <div class="team">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/st_louis_rams.png">
                        <p><strong>2012 - 2016</strong></p>
                        <p>St. Louis/Los Angeles Rams </p>
                    </div>
                    <div class="team">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/tennessee_titans.png">
                        <p><strong>2001 - 2010</strong></p>
                        <p>Tennessee Titans </p>
                    </div>


                </div>

            </div>
            
            

        
        </div>
        <?php echo do_shortcode('[URIS id=782]'); ?>
                <div class="container">
            <div>
                <h1>Chuck's Playing Career</h1>

                <div class="career player">
                    <div class="team">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/houston-oilers.png">
                        <p><strong>1995</strong></p>
                        <p>Houston Oilers</p>
                    </div>
                    <div class="team">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/arizona_cardinals.png">
                        <p><strong>1993</strong></p>
                        <p>Phoenix Cardinals </p>
                    </div>
                    <div class="team">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/greenbay.png">
                        <p><strong>1988-1992</strong></p>
                        <p>Green Bay Packers </p>
                    </div>
                    
                                        <div class="team">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/wildcats.png">
                                            <p><strong>1985-1988</strong></p>
                        <p>University of Arizona</p>
                    </div>
                </div>

            </div>
            

        
        </div>
	<?php echo do_shortcode('[URIS id=772]'); ?>
	<!-- INTRO SECTION END -->

	<!-- INTRO SECTION -->
        <div class="container">
            <div>
                <h1>Honors & Awards</h1>

                <div class="career awards">
                    <div class="team">
                        <p><strong>2010</strong></p>
                        <p>NCAA College Football Hall of Fame Induction</p>
                    </div>
                    <div class="team">
                        <p><strong>1993</strong></p>
                        <p>University of Arizona Sports Hall of Fame Inductee</p>
                    </div>
                                        <div class="team">
                        <p><strong>1992</strong></p>
                        <p>NFL Pro Bowl Selection</p>
                    </div>
                                        <div class="team">
                        <p><strong>1991</strong></p>
                        <p>NFL All-Madden Selection</p>
                    </div>
                    
                                        <div class="team">
                        <p><strong>1988</strong></p>
                        <p>Pac-10 Conference Medal winner</p>
                    </div>
                    <div class="team">
                        <p><strong>1988</strong></p>
                        <p>NCAA Today's Top VI Award recipient</p>
                    </div>
                                        <div class="team">
                        <p><strong>1987</strong></p>
                        <p>Consensus All-American </p>
                    </div>
                                        <div class="team">
                        <p><strong>1987</strong></p>
                        <p>UPI, Football News, Kodak, Walter Camp and coaches All-America selection</p>
                    </div>
                    
                                                            <div class="team">
                        <p><strong>1986-87</strong></p>
                        <p>Jim Thorpe Award Semifinalist</p>
                    </div>
                    <div class="team">
                        <p><strong>1987</strong></p>
                        <p>Pac-10 Defensive Player of the Year</p>
                    </div>
                                        <div class="team">
                        <p><strong>1986, 1987</strong></p>
                        <p>2-time Academic All-America first team </p>
                    </div>
                    
                                                            <div class="team">
                        <p><strong>1986, 1987</strong></p>
                        <p>2-time All-Pac-10 first team</p>
                    </div>
                    
                    <div class="team">
                        <p><strong>1986</strong></p>
                        <p>Aloha Bowl MVP </p>
                    </div>
                    
                    <div class="team">
                        <p><strong>1985</strong></p>
                        <p>Academic All-America second team</p>
                    </div>
                </div>

            </div>
            
            

        
        </div>

	<!-- INTRO SECTION END -->
      </section>

<?php
get_footer();
