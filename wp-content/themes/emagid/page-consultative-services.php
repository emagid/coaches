<?php

get_header(); ?>


	<!-- HERO SECTION -->
	<section class='hero home_hero' style="background-image:url(<?php the_field('banner'); ?>)">
        <div class="overlay">
            <div class='text_box'>
                <h1 class="margin_reduce">Business and Administrative Needs at Your Service</h1>
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/network.png">
<!--                <a href='#' class='button'> View More </a>-->
            </div>
        </div>
	</section>

	<!-- HERO SECTION END -->


    <section class='icons'>
        <div class="container">
            <div class="icon_group">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon_1.png">
<!--                Accounting and Financial Services-->
                <p><?php echo do_shortcode('[popup_anything id="336"]'); ?></p>
            </div>
                        <div class="icon_group">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/server.png">
<!--                Technology-->
                <p><?php echo do_shortcode('[popup_anything id="337"]'); ?></p>
            </div>
                        <div class="icon_group">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon_2.png">
<!--                Analytical and Quality Reporting-->
                <p><?php echo do_shortcode('[popup_anything id="328"]'); ?></p>
            </div>
            <div class="icon_group">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon_3.png">
<!--                Malpractice Management-->
                <p><?php echo do_shortcode('[popup_anything id="329"]'); ?></p>
            </div>
            
                        <div class="icon_group">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon_4.png">
<!--                Human Resources/ Benefits-->
                            <p><?php echo do_shortcode('[popup_anything id="330"]'); ?></p>
            </div>
                        <div class="icon_group">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon_5.png">
<!--                NCQA/PCMH Certification-->
                            <p><?php echo do_shortcode('[popup_anything id="331"]'); ?></p>
            </div>
            <div class="icon_group">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon_6.png">
<!--                Billing Support-->
                <p><?php echo do_shortcode('[popup_anything id="332"]'); ?></p>
            </div>
            
                        <div class="icon_group">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon_7.png">
<!--                Allied Telehealth Program-->
                            <p><?php echo do_shortcode('[popup_anything id="333"]'); ?></p>
            </div>
                        <div class="icon_group">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon_8.png">
<!--                Marketing/PR-->
                            <p><?php echo do_shortcode('[popup_anything id="334"]'); ?></p>
            </div>
            <div class="icon_group">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/icon_9.png">
<!--                Adjuvant.Health Office Manager Enrichment Programs-->
                <p><?php echo do_shortcode('[popup_anything id="335"]'); ?></p>
            </div>
        </div>
        
	</section>
  <script type="text/javascript">
        $("a.button").click(function(event) {
            event.preventDefault();
            $('html,body').animate({
                scrollTop: $(".icons").offset().top},
                'slow');
        });
  </script>

<?php
get_footer();