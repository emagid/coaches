<?php
/**
 * The template for displaying all pages
 * 
 * Template Name: Contact Page
 * 
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package emagid
 */

get_header(); ?>


	<!-- HERO SECTION -->
	<section class='hero home_hero' style="background-image:url(<?php the_field('banner'); ?>)">
        <div class="overlay">
            <div class='text_box'>
                <h1><?php the_field('title'); ?></h1>
                <a href='#' class='button'>Contact Us</a>
            </div>
        </div>
	</section>
    <div class="contact_map" style="background-image:url(http://www.teamsm2.com/wp-content/uploads/2017/11/manhattanbeach.jpg)">
    <div class="contact_box">
        <h3>You can also reach us by</h3>
        <p><strong>Phone:</strong> 424.226.5434</p>
        <p><strong>Email:</strong> info@sociallyready.com</p>
    </div>
</div>
	<!-- HERO SECTION END -->

    <section class='bio'>
        <div class="contact_form">
            <p><?php the_field('subtitle'); ?></p>
            <?php the_field('content'); ?>
        </div>
	</section>



  <script type="text/javascript">
        $("a.button").click(function(event) {
            event.preventDefault();
            $('html,body').animate({
                scrollTop: $(".bio").offset().top},
                'slow');
        });
  </script>
<?php
get_footer();
