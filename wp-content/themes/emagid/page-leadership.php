<?php

get_header(); ?>


	<!-- HERO SECTION -->
	<section class='hero home_hero' style="background-image:url(<?php the_field('banner'); ?>)">
        <div class="overlay">
            <div class='text_box'>
                <h1>The leaders behind it all.</h1>
                <a href='#' class='button'> Meet The Team</a>
            </div>
        </div>
	</section>

	<!-- HERO SECTION END -->


    <section class='icons ' id="headshots">
        <div class="container" id="headshot_container">
            <p><?php the_field('content'); ?></p><br><br>
                        <?php
	  			$args = array(
	    		'post_type' => 'members',
                    'order' => 'DESC',
                    'posts_per_page' => 99
                    
	    		);
	  			$products = new WP_Query( $args );
	  				if( $products->have_posts() ) {
	    			while( $products->have_posts() ) {
	      		$products->the_post();
			?> 

            <div class="icon_group">
                <div class="visual">
                    <a href="#<?php the_field('unique_id'); ?>">
                <img src="<?php the_field('headshot'); ?>">
                <p><span><?php the_field('name'); ?></span></p>
                <p><?php the_field('title'); ?></p>
                    </a>
                </div>


                <div id="<?php the_field('unique_id'); ?>" class="modalbg">
                  <div class="dialog">
                    <a href="#close" title="Close" class="close">X</a>
                                      <div class="visual">

                <img src="<?php the_field('headshot'); ?>">
                <p><span><?php the_field('name'); ?></span></p>
                            <p><?php the_field('title'); ?></p>
    
                </div>
                    <p><?php the_field('bio'); ?></p>
                    </div>
                </div>
            </div>

            <?php
			}
				}
			else {
			echo 'No Members Found';
			}
		?>         
        
                      
        </div>
        

        
	</section>

  <script type="text/javascript">
        $("a.button").click(function(event) {
            event.preventDefault();
            $('html,body').animate({
                scrollTop: $(".icons").offset().top},
                'slow');
        });
  </script>
<?php
get_footer();