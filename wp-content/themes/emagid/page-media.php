<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package emagid
 */

get_header(); ?>



<section class="press" style="padding-top:150px;">
    <h3>News & Press</h3>
<div class="news_wrapper">
                    <?php
	  			$args = array(
	    		'post_type' => 'press',
                    'posts_per_page' => 99
	    		);
	  			$products = new WP_Query( $args );
	  				if( $products->have_posts() ) {
	    			while( $products->have_posts() ) {
	      		$products->the_post();
			?> 
        
        <div class="news_box">
            <div class="press_image" style="background-image:url(<?php the_field('featured_image'); ?>);"></div>
            <h3><?php the_field('title'); ?></h3>
            <a href="<?php the_permalink(); ?>"><button>Read Now</button></a>
        </div>
        
        <?php
			}
				}
			else {
			echo 'No Press Found';
			}
		?>   
</div>
</section>

<?php
get_footer();
