<?php

get_header(''); ?>



	<!-- HERO SECTION -->
	<section class='hero home_hero' style="background-image:url(<?php the_field('banner'); ?>)">
        <div class="overlay">
            <div class='text_box'>
                <h1>News</h1>
<!--                <a href='' class='button'> Read More </a>-->
            </div>
        </div>
	</section>

	<!-- HERO SECTION END -->



<section class="press">
    <h3><?php the_field('intro_header'); ?></h3>
<div class="news_wrapper">
                    <?php
	  			$args = array(
	    		'post_type' => 'press',
                    'posts_per_page' => 4
	    		);
	  			$products = new WP_Query( $args );
	  				if( $products->have_posts() ) {
	    			while( $products->have_posts() ) {
	      		$products->the_post();
			?> 
        
        <div class="news_box">
            <div class="press_image" style="background-image:url(<?php the_field('featured_image'); ?>);"></div>
            <h3><?php the_field('title'); ?></h3>
            <a href="<?php the_permalink(); ?>"><button>View Article</button></a>
        </div>
        
        <?php
			}
				}
			else {
			echo 'No Press Found';
			}
		?>   
<!--    <a href="/media" class="button"> Read More </a>-->
    
    <?php wp_reset_query();	 // Restore global post data stomped by the_post(). ?>
</div>
</section>

<?php
get_footer();
