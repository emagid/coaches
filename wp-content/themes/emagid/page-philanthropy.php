<?php
/**
 * The template for displaying all pages
 *
 *  Template Name: About Page
 * 
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package emagid
 */

get_header('home'); ?>



	<!-- HERO SECTION -->
	<section class='hero home_hero' style="background-image:url(<?php the_field('banner'); ?>)">
        <div class="overlay">
            <div class='text_box'>
                <h1><?php the_field('intro_header'); ?></h1>
<!--                <a href='' class='button'> Read More </a>-->
            </div>
        </div>
	</section>

	<!-- HERO SECTION END -->



	<!-- INTRO SECTION -->
	<section class='intro'>
        <div class="container">
            <div>
                <h1><?php the_field('intro_header'); ?></h1>

                <div class="intro_about">
                    <?php the_field('intro_section'); ?>
                    </div>
          
            </div>

        
        </div>
	</section>
	<!-- INTRO SECTION END -->



  <script type="text/javascript">
        $("a.button").click(function(event) {
            event.preventDefault();
            $('html,body').animate({
                scrollTop: $(".intro").offset().top},
                'slow');
        });
  </script>

<?php
get_footer();
