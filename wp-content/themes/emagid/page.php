<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package emagid
 */

get_header(); ?>

<?php

get_header(); ?>

	<!-- HERO SECTION -->
	<section class='hero home_hero' style="background-image:url(<?php the_field('banner'); ?>)">
        <div class="overlay">
            <div class='text_box'>
                <h1>The leaders behind it all.</h1>
                <a href='#' class='button'> Meet The Team</a>
            </div>
        </div>
	</section>

	<!-- HERO SECTION END -->

<div class="news_wrapper">
    <?php echo do_shortcode('[display-posts image_size="medium" include_excerpt="true" include_date="true" excerpt_more="Read More" excerpt_more_link="true"]'); ?>
</div>

  <script type="text/javascript">
        $("a.button").click(function(event) {
            event.preventDefault();
            $('html,body').animate({
                scrollTop: $(".icons").offset().top},
                'slow');
        });
  </script>
<?php
get_footer();

<?php
get_footer();
