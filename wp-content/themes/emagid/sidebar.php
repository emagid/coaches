<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package emagid
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>

<aside id="secondary" class="widget-area">
	                    <?php
	  			$args = array(
	    		'post_type' => 'press',
                    'posts_per_page' => 99
	    		);
	  			$products = new WP_Query( $args );
	  				if( $products->have_posts() ) {
	    			while( $products->have_posts() ) {
	      		$products->the_post();
			?> 
        
    <ul class="blog_posts">
        <li><a href="<?php the_permalink(); ?>"><p><?php the_field('title'); ?></p></a></li>
    </ul>
        
        <?php
			}
				}
			else {
			echo 'No Press Found';
			}
		?>   
</aside><!-- #secondary -->
