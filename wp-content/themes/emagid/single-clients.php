<?php
/**
 * The template for displaying all pages
 *
 *  Template Name: Case Studies Page
 * 
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package emagid
 */

get_header(); ?>


	<!-- HERO SECTION -->
	<section class='hero home_hero' style="background-image:url(<?php the_field('background'); ?>)">
        <div class="overlay">
            <div class='text_box'>
                <h1><?php the_field('company_name'); ?></h1>
<!--                <a href='/contact' class='button'> JOIN THE CLUB </a>-->
            </div>
        </div>
	</section>
<!--
    <section class="small_hero" style="background-image:url(<?php the_field('banner_small'); ?>)">
        <div class="overlay">
            <div class='text_box' style="text-align: left; width: 80%; margin: 0 auto;">
                <p id="case_studies_p"><strong>ACQUIRED IN 2018.</strong> THIS COMAPANY LOREM IPSUM DOLOR SIT AMET, CONSECTETUR ADIPISCING ELIT. VESTIBULUM SEMPER PRETIUM ALIQUET.</p>
            </div>
        </div>
    </section>
-->

	<!-- HERO SECTION END -->
    <section class='bio'>
        <h2 class="auto_center"><?php the_field('company_name'); ?></h2>
        <?php the_field('content'); ?>
	</section>

<?php
get_footer();
