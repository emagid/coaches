<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package emagid
 */

get_header(); ?>
<div class="blog_post">
	<div class='blog_hero'>
        <div class="overlay">
            <div class='text_box'>
                <h1><?php single_post_title(); ?></h1>
            </div>
        </div>
	</div>
<div class="news_wrapper_single">
<div class="wrapper">
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
 
    <h5><?php echo get_the_date(); ?></h5>
    <?php the_post_thumbnail(); ?>
 <?php the_content(); ?>


<?php endwhile; ?>
<?php endif; ?>
</div>
</div>
    </div>
<?php
get_sidebar(); ?>
<?php
get_footer();
